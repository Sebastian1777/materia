
package materia;
public class Materia {

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getTalleres() {
        return talleres;
    }

    public void setTalleres(double talleres) {
        this.talleres = talleres;
    }

    public double getExamenes() {
        return examenes;
    }

    public void setExamenes(double examenes) {
        this.examenes = examenes;
    }

    public double getNotafinal() {
        return notafinal;
    }

    public void setNotafinal(double notafinal) {
        this.notafinal = notafinal;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }
    public void calcularTalleres(double t1,double t2, double t3){
        this.talleres=(t1+t2+t3)/3*0.4;
        
    }
    public void calcularExamenes(double e1,double e2){
         this.examenes=(e1+e2)/2*0.6;
    }
    public void calcularFinal(double talleres,double examenes){
        this.notafinal=notafinal+talleres+examenes;
    }
    
     public String mostrarConcepto(){
         if (this.notafinal<3.5){
             System.out.println("REPROBO su materia"); 
         }
         else {
             System.out.println("APROBO su materia");
         }
         
        return " su materia es: "+getNombre()+" y su nota final es: "+getNotafinal();
    }
     
    public Materia(String nombre){
        this.nombre=nombre;
        this.examenes=0;
        this.notafinal=0;
        this.talleres=0;
        this.concepto=null;
        
        
        
    }
    //atributos
    private String nombre;
    private double talleres;
    private double examenes;
    private double notafinal;
    private String concepto;

 
    public static void main(String[] args) {
        Materia estudiante1=new Materia("matematicas");
        
        estudiante1.calcularTalleres(4.0, 3.0, 2.0);
       // System.out.println(estudiante1.mostrarConcepto());
        
        estudiante1.calcularExamenes(4.5, 3.5);
        //System.out.println(estudiante1.mostrarConcepto());
        
        estudiante1.calcularFinal(estudiante1.getTalleres(), estudiante1.getExamenes());
        System.out.println(estudiante1.mostrarConcepto());
          
      
    }
    
}
